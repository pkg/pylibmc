pylibmc (1.5.2-3apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:18:33 +0200

pylibmc (1.5.2-3) unstable; urgency=medium

  * Drop python2 support; Closes: #937468

 -- Sandro Tosi <morph@debian.org>  Fri, 27 Mar 2020 22:57:48 -0400

pylibmc (1.5.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Sandro Tosi ]
  * Build documentation with python3 sphinx

 -- Sandro Tosi <morph@debian.org>  Sat, 01 Feb 2020 13:17:16 -0500

pylibmc (1.5.2-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.0.0.
  * Drop -dbg packages in favour of -dbgsym.

 -- Michael Fladischer <fladi@debian.org>  Mon, 24 Jul 2017 14:55:27 +0200

pylibmc (1.5.1-1) unstable; urgency=low

  [ Michael Fladischer ]
  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Add bindnow to hardening flags.
  * Add patch to fix spelling of occurred in src/_pylibmcmodule.c.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

 -- Michael Fladischer <fladi@debian.org>  Fri, 27 May 2016 10:03:07 +0200

pylibmc (1.5.0-4) unstable; urgency=medium

  [ Michael Fladischer ]
  * Disable tests as they require a running memcached server (Closes:
    #805227).

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Sun, 22 Nov 2015 16:36:45 +0100

pylibmc (1.5.0-3) unstable; urgency=medium

  * Team upload.
  * Update sap-theme for Sphinx 1.3 (Closes: #799576).
  * Bump python3-sphinx build-dependency.

 -- Michael Fladischer <fladi@debian.org>  Mon, 21 Sep 2015 09:33:27 +0200

pylibmc (1.5.0-2) unstable; urgency=medium

  * Team upload.
  * Remove overrides for dh_python2 and dh_python3 (Closes: #793333).
    Thanks to Steve Langasek for the patch.

 -- Michael Fladischer <fladi@debian.org>  Wed, 12 Aug 2015 13:14:41 +0200

pylibmc (1.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Tue, 30 Jun 2015 08:05:23 +0200

pylibmc (1.4.3-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Add Python3 support through a separate package.
  * Build documentation an ship it in a separate package.
  * Bump Standards-Version to 3.9.6.
  * Switch buildsystem to pybuild.
  * Add dh-python to Build-Depends.
  * Use pypi.debian.net service for uscan.
  * Convert d/copyright to DEP5 format.
  * Drop versioned dependency on python-all-dev and python-all-dbg.
  * Reformat d/control using cme.
  * Override dh_compress to exclude changelog.html.

 -- Michael Fladischer <fladi@debian.org>  Thu, 25 Jun 2015 20:14:47 +0200

pylibmc (1.2.3-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Carl Chenet ]
  * New upstream release
    - debian/control
      - bump Standards-Version to 3.9.4.0
      - bump python minimum version to 2.6
    - debian/compat
      - bump to 9

 -- Carl Chenet <chaica@ohmytux.com>  Wed, 19 Jun 2013 23:31:07 +0200

pylibmc (1.2.2-1) unstable; urgency=low

  * New upstream version
  * debian/copyright
    - add the copyright of a new file
  * debian/control
    - bump Standards-Version to 3.9.2.0
    - removed python-support
    - replaced XS-Python-Version to X-Python-Version
  * debian/rules
    - added --with python2 to dh $@
  * debian/install
    - corrected paths
  * debian/python-pylibmc-dbg.install
    - corrected paths

 -- Carl Chenet <chaica@ohmytux.com>  Wed, 07 Dec 2011 00:24:03 +0100

pylibmc (1.1.1-1) unstable; urgency=low

  * New upstream version
  * debian/install
    - added a more flexible syntax to copy .egg file

 -- Carl Chenet <chaica@ohmytux.com>  Tue, 15 Jun 2010 00:22:39 +0200

pylibmc (1.0-1) unstable; urgency=low

  * New upstream version
  * debian/source/format
    - bump to 3.0 source format
  * debian/control
    - switched Maintainer to DPMT
    - switched Uploaders to Carl Chenet <chaica@ohmytux.com>
    - bump Standards-Version to 3.8.4
    - added zlib1g-dev in Build-Depends for newly introduced
      compression support
  * debian/install
    - added egg-info file (Closes: #578092)

 -- Carl Chenet <chaica@ohmytux.com>  Mon, 24 May 2010 23:09:52 +0200

pylibmc (0.9.2-1) unstable; urgency=low

  * Initial release. (Closes: #556901)

 -- Carl Chenet <chaica@ohmytux.com>  Sat, 28 Nov 2009 16:33:10 +0100
